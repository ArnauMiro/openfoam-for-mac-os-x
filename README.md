# OPENFOAM FOR MACOSX

### How do I get set up? ###

1. Install xcode and the Command Line Tools for OSX by:
Note: xcode can be download for free from appstore

Command line tools are provided for OS 10.9 in the ‘Dependencies’ folder

Then accept xcode license:

$ sudo xcodebuild -license

2. Install MacPorts (inside ‘Dependencies’ folder)

3. Build and install dependencies:

$ sudo port selfupdate

$ sudo port install gcc46 openmpi boost cgal ccache flex bison scotch

It might take a while

## mpicc and mpirun fix ##
mpicc and mpirun are installed with another name from macports. For this reason run:

$ sudo cp /opt/local/bin/mpicc-openmpi-mp /opt/local/bin/mpicc
$ sudo cp /opt/local/bin/mpirun-openmpi-mp /opt/local/bin/mpirun

4. Change directory to the openFOAM directory

$ cd /Volumes/OpenFOAM-v2.3.0/OpenFOAM-2.3.0

** Fix to use paraFoam **
paraFoam requires paraview to be installed in your computer. Paraview is provided in the ‘Tools’ folder. Just drag and drop into the Applications folder.

5. Double click on OpenFOAM app to run openfoam in a terminal

** Alternatively, source bashrc to get access to all OpenFOAM binaries **

$ source etc/bashrc

Now you should be able to use OpenFOAM in you OS X system. 

** Note that every time you open a terminal you need to repeat step 5 in order to get the binaries in your path. **

Alternatively, it can be added in your .profile file located in the $HOME folder.

$ open .profile

and add the line at the end

source /Volumes/OpenFOAM-v2.3.0/OpenFOAM-2.3.0/etc/bashrc

Every time a terminal is open will source the files of the image. (Requires the image to be mounted)

### Who do I talk to? ###
Any comments/suggestions can be made to: arnaumiro(at)gmail(dot)com