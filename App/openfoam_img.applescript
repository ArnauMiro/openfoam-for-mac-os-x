on run {input, parameters}
	
	tell application "Terminal"
		reopen
		activate
		do script "source /Volumes/OpenFOAM-v2.3.0/OpenFOAM-2.3.0/etc/bashrc" in window 1
		do script "source /Volumes/OpenFOAM-v2.3.0/foam-extend-3.1/etc/bashrc" in window 1
		do script "source /Volumes/OpenFOAM-v2.3.0/.bashrc" in window 1
		do script "clear" in window 1
	end tell
	
	return input
	
end run