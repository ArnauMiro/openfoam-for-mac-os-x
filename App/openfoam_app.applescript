on run {input, parameters}
	
	tell application "Finder"
		set AppPath to POSIX path of ((container of (path to me)) as text)
	end tell
	
	tell application "Terminal"
		reopen
		activate
		do script "export foamInstall=" & AppPath & "OpenFOAM.app" in window 1
		do script "source $foamInstall/OpenFOAM-2.3.0/etc/bashrc" in window 1
		do script "source $FOAM_INST_DIR/foam-extend-3.1/etc/bashrc" in window 1
		do script "source $FOAM_INST_DIR/.bashrc" in window 1
		do script "clear" in window 1
	end tell
	
	return input
end run